import { useState, useEffect } from "react";
import yelp from "../api/yelp";

export default () => {
  const [results, setResults] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const searchApi = async (searchTerm) => {
    console.log("request");
    try {
      const response = await yelp.get("/search", {
        params: {
          limit: 50,
          term: searchTerm,
          location: "san jose",
        },
      });

      setResults(response.data.businesses);
    } catch (err) {
      console.error(err);

      setErrorMessage("Something went wrong");
    }
  };

  useEffect(() => {
    searchApi("fish");
  }, [errorMessage, results.length]);

  return [results, errorMessage, searchApi];
};
